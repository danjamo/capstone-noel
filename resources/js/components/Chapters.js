import React, { Component } from 'react';
import { Button } from 'reactstrap';

export default class Chapters extends Component {
    state = {
        counter: 0
    };

    handleAddChapter = () => {
        this.setState({
            counter: this.state.counter + 1
        });
    };

    render() {
        const child = [];

        for (var i = 0; i < this.state.counter; i += 1) {
            child.push(<Child key={i} number={i} />);
        }
        return <Parent addChapter={this.handleAddChapter}>{child}</Parent>;
    }
}

const Parent = props => (
    <div>
        <div>
            <Button onClick={props.addChapter}>Append Chapter</Button>
        </div>
        <hr />
        {/* props.children is a react default */}
        <div>{props.children}</div>
    </div>
);

const Child = props => (
    <div className='border bg-white p-3'>
        <h5 className='mb-0'>Child {props.number} of Parent</h5>
    </div>
);

// export default class Chapters extends Component {
//     state = {
//         numChildren: 0
//     };

//     onAddChild = () => {
//         this.setState({
//             numChildren: this.state.numChildren + 1
//         });
//     };

//     render() {
//         const children = [];

//         for (var i = 0; i < this.state.numChildren; i += 1) {
//             children.push(<ChildComponent key={i} number={i} />);
//         }

//         return (
//             <ParentComponent addChild={this.onAddChild}>
//                 {children}
//             </ParentComponent>
//         );
//     }
// }

// const ParentComponent = props => (
//     <div className='card calculator'>
//         <p>
//             <a href='#' onClick={props.addChild}>
//                 Add Another Child Component
//             </a>
//         </p>
//         <div id='children-pane'>{props.children}</div>
//     </div>
// );

// const ChildComponent = props => <div>{'I am child ' + props.number}</div>;
