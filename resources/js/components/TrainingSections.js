import React from 'react';
import Axios from 'axios';
import NoDataImg from '../../img/undraw_no_data.svg';
import ComponentError from '../../img/bug_fix.svg';
import styled from 'styled-components';
import { Button } from 'reactstrap';
import { Droppable } from 'react-beautiful-dnd';
import { Draggable } from 'react-beautiful-dnd';

const SectionContainer = styled.li`
    margin: 0.25rem 0;
    background-color: #ebedef;
`;

const ChapterContainer = styled.div`
    margin: 0.25rem 0;
    background-color: #a5d6a7;
`;

const ChapterDraggables = styled.div``;

const ChapterItemContainer = styled.div`
    padding: 0.75rem 0.75rem;
    background-color: white;
    border: 1px solid lightgrey;
`;

const ErrorMessage = styled.h1`
    position: absolute;
    padding: 0.5rem 0.75rem;
    font-size: 4vw;
    top: 50%;
    left: 50%;
    background: rgba(0, 0, 0, 0.4);
    transform: translate(-50%, -50%);
    @media (min-width: 768px) {
        font-size: 22px;
    }
`;

const ErrorMessageContainer = styled.div`
    text-align: center;
    position: relative;
    color: white;
`;

export default class TrainingSections extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        // console.log('rendered TS');
        if (!this.props.sections || this.props.sections.length == 0) {
            return (
                <ul className='list-group mt-3'>
                    <div className='text-center'>
                        <p className='lead text-muted'>No sections added yet</p>
                        <img src={NoDataImg} className='w-25 h-auto' />
                    </div>
                </ul>
            );
        }
        const sections = this.props.sections;
        const sectionItems = sections.map(section => (
            <Section
                key={section.id}
                section={section}
                getSectionId={this.props.getSectionId}
                deleteSection={this.props.deleteSection}
            />
        ));
        return <ul className='list-group'>{sectionItems}</ul>;
    }
}

class Section extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false
        };
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    componentDidCatch(error, errorInfo) {
        // You can also log the error to an error reporting service
        console.log(error, errorInfo);
    }

    render() {
        const section = this.props.section;
        // console.log(this.props);
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return (
                <ErrorMessageContainer>
                    <img
                        src={ComponentError}
                        alt=''
                        className='img-fluid w-50'
                    />
                    <ErrorMessage>Sorry! Something went wrong</ErrorMessage>
                </ErrorMessageContainer>
            );
        }

        if (!section.chapters || section.chapters.length === 0) {
            return (
                <SectionContainer className='list-group-item'>
                    <div className='d-flex justify-content-between align-items-center'>
                        {section.title}
                        <Button
                            color='danger'
                            size='sm'
                            onClick={this.props.deleteSection.bind(
                                this,
                                section.id
                            )}
                        >
                            X
                        </Button>
                    </div>
                    <div className='mt-2 d-block flex-row'>
                        <Button
                            color='secondary'
                            outline
                            size='sm'
                            className='w-50'
                            onClick={this.props.getSectionId.bind(
                                this,
                                section.id
                            )}
                        >
                            Add Chapter
                        </Button>
                        <Button
                            color='secondary'
                            outline
                            size='sm'
                            className='w-50'
                        >
                            Add test
                        </Button>
                    </div>
                    <hr />
                    <div className='text-center text-muted font-italic h6'>
                        Section is empty
                    </div>
                </SectionContainer>
            );
        } else {
            const chapterItems = section.chapters.map((chapter, index) => (
                <Chapters
                    key={chapter.id}
                    getSectionId={this.props.getSectionId}
                    chapter={chapter}
                    index={index}
                />
            ));
            return (
                <SectionContainer className='list-group-item'>
                    <div className='d-flex justify-content-between align-items-center'>
                        {section.title}
                        <Button
                            color='danger'
                            size='sm'
                            onClick={this.props.deleteSection.bind(
                                this,
                                section.id
                            )}
                        >
                            X
                        </Button>
                    </div>
                    <div className='mt-2 d-block flex-row'>
                        <Button
                            color='secondary'
                            outline
                            size='sm'
                            className='w-50'
                            onClick={this.props.getSectionId.bind(
                                this,
                                section.id
                            )}
                        >
                            Add Chapter
                        </Button>
                        <Button
                            color='secondary'
                            outline
                            size='sm'
                            className='w-50'
                        >
                            Add test
                        </Button>
                    </div>
                    <hr />
                    <Droppable droppableId={section.id}>
                        {provided => (
                            <ChapterContainer
                                className='list-group'
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                            >
                                {chapterItems}
                                {provided.placeholder}
                            </ChapterContainer>
                        )}
                    </Droppable>
                </SectionContainer>
            );
        }
    }
}

// Functional Chapters Component
// const Chapters = props => (
//     <ChapterDraggables>
//         <div className='row'>
//             <div className='col'>
//                 <span className='h5'>{props.chapter.index}</span>
//             </div>
//             <div className='col'>{props.chapter.title}</div>
//         </div>
//     </ChapterDraggables>
// );

class Chapters extends React.PureComponent {
    render() {
        return (
            <ChapterDraggables>
                <Draggable
                    draggableId={this.props.chapter.id}
                    index={this.props.chapter.index}
                >
                    {provided => (
                        <ChapterItemContainer
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            ref={provided.innerRef}
                        >
                            <div className='row'>
                                <div className='col'>
                                    <span className='h5'>
                                        {this.props.chapter.index}
                                    </span>
                                </div>
                                <div className='col'>
                                    {this.props.chapter.title}
                                </div>
                            </div>
                        </ChapterItemContainer>
                    )}
                </Draggable>
            </ChapterDraggables>
        );
    }
}
