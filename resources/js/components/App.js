import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './Header';
import TrainingsList from './TrainingsList';
// import NewTraining from './NewTraining';
import Training from './Training';
import Chapters from './Chapters';

class App extends Component {
    render() {
        return (
            <Router>
                <Header />
                <Switch>
                    <Route exact path='/' component={TrainingsList} />
                    {/* <Route path='/create' component={NewTraining} /> */}
                    <Route path='/test' component={Chapters} />
                    <Route path='/:id' component={Training} />
                </Switch>
            </Router>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
