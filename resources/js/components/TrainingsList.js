import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import Axios from 'axios';
import styled from 'styled-components';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Spinner
} from 'reactstrap';

// import FormModal from './FormModal';

const SpinnerContainer = styled.div`
    position: fixed;
    z-index: 1031;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const CancelToken = Axios.CancelToken;
const source = CancelToken.source();

export default class TrainingsList extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            trainings: null,
            isModalOpen: false
        };
        this.redirectUser = this.redirectUser.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;

        Axios.get('/api/trainings', {
            cancelToken: source.token
        })
            .then(response => {
                if (this._isMounted) {
                    this.setState({
                        trainings: response.data
                    });
                }
            })
            .catch(thrown => {
                if (axios.isCancel(thrown)) {
                    console.log('Request canceled', thrown.message);
                } else {
                    console.log('Server response error');
                }
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    redirectUser = id => {
        // console.log(this.props, id);
        const { history } = this.props;
        history.push(`/${id}`);
    };

    render() {
        const { trainings } = this.state;
        return (
            <div>
                {this.state.trainings ? (
                    <div className='container py-3'>
                        <div className='row justify-content-center'>
                            <div className='col-md-10'>
                                <div className='card'>
                                    <div className='card-header'>
                                        Manage Trainings
                                    </div>
                                    <div className='card-body'>
                                        <FormModal
                                            buttonLabel='Add New Training'
                                            btnClassname='mb-3'
                                            modal={this.state.isModalOpen}
                                            hasSubmitted={this.redirectUser}
                                            autoFocus={true}
                                        />
                                        <div className='small'>
                                            List of Trainings
                                        </div>
                                        <ul className='list-group list-group-flush'>
                                            {trainings.map(training => (
                                                <Link
                                                    className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                                                    to={`/${training.id}`}
                                                    key={training.id}
                                                >
                                                    {training.title}
                                                    <span className='badge badge-primary badge-pill'>
                                                        {
                                                            training.sections_count
                                                        }
                                                    </span>
                                                </Link>
                                            ))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className='d-flex justify-content-center position-relative'>
                        <SpinnerContainer>
                            <Spinner
                                color='secondary'
                                style={{ width: '5rem', height: '5rem' }}
                                type='grow'
                            />
                        </SpinnerContainer>
                    </div>
                )}
            </div>
        );
    }
}

class FormModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            title: '',
            errors: []
        };
        this.toggle = this.toggle.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleCreateTraining = this.handleCreateTraining.bind(this);
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
    }

    // componentDidMount = () => {
    //     console.log('I am mounted');
    // };

    // componentWillUnmount() {
    //     this._isMounted = false;
    // }

    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    };

    handleFieldChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleCreateTraining = event => {
        event.preventDefault();
        const training = {
            title: this.state.title
        };

        Axios.post('api/trainings', training, {
            cancelToken: source.token
        })
            .then(response => {
                alert('Training successfully created!');
                // Call parent function
                this.toggle();
                this.props.hasSubmitted(response.data.id);
            })
            .catch(error => {
                if (error.response) {
                    // Populate the errors state
                    this.setState({
                        errors: error.response.data.errors
                    });
                }
            });
    };

    hasErrorFor = field => {
        return !!this.state.errors[field];
    };

    renderErrorFor = field => {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    };

    render() {
        return (
            <div>
                <Button
                    color={this.props.color ? this.props.color : 'primary'}
                    onClick={this.toggle}
                    className={this.props.btnClassname}
                    size={this.props.btnSize}
                    outline={this.props.btnOutline}
                    block={this.props.btnBlock}
                >
                    {this.props.buttonLabel}
                </Button>
                <Modal
                    isOpen={this.state.modal}
                    toggle={this.toggle}
                    autoFocus={!this.props.autoFocus}
                >
                    <form onSubmit={this.handleCreateTraining}>
                        <ModalHeader toggle={this.toggle}>
                            New Training Title
                        </ModalHeader>
                        <ModalBody>
                            <div className='form-group'>
                                <label htmlFor='title'>Training Title</label>
                                <input
                                    type='text'
                                    name='title'
                                    id='title'
                                    className={`form-control ${
                                        this.hasErrorFor('title')
                                            ? 'is-invalid'
                                            : ''
                                    }`}
                                    value={this.state.title}
                                    onChange={this.handleFieldChange}
                                    autoFocus={this.props.autoFocus}
                                />
                                {this.renderErrorFor('title')}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color='primary'>Create</Button>
                            {/* <Button color='secondary' onClick={this.toggle}>
                                Cancel
                            </Button> */}
                        </ModalFooter>
                    </form>
                </Modal>
            </div>
        );
    }
}
