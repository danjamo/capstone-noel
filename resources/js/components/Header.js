import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../logos/nltd_logo.png';
import Sample from '../../img/mememe.jpg';

export default class Header extends Component {
    render() {
        return (
            <div className='d-block shadow-sm bg-white border-bottom'>
                <div className='container'>
                    <div className='d-flex flex-row align-items-center'>
                        <div className='navbar flex-fill px-0'>
                            <div className='flex-grow-1'>
                                <Link className='navbar-brand' to='/'>
                                    <img
                                        src={Logo}
                                        width='50'
                                        height='50'
                                        alt='Noel Logo'
                                    />
                                </Link>
                            </div>
                            <button className='btn btn-primary mx-3'>
                                Notif
                            </button>
                            <span>User Name</span>
                            <img
                                src={Sample}
                                width='50'
                                height='50'
                                className='ml-3'
                            />
                            <div className='ml-2'>V</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
