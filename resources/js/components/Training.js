import Axios from 'axios';
import React, { Component } from 'react';
import TrainingSections from './TrainingSections';
import styled from 'styled-components';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { DragDropContext } from 'react-beautiful-dnd';

// const EditDescBtn = styled.button`
//     display: none;
//     position: absolute;
//     right: 0;
//     top: 50%;
// `;

const TrainingDescContainer = styled.div`
    /* background-color: #c6e1f3; */
    position: relative;
`;

export default class Training extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            training: [],
            section_title: '',
            sections: [],
            // chapters: [],
            errors: [],
            chapter_title: '',
            isModalOpen: false,
            section_id: ''
        };
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.handleAddNewSection = this.handleAddNewSection.bind(this);
        this.handleSectionDelete = this.handleSectionDelete.bind(this);
        this.handleCreateChapter = this.handleCreateChapter.bind(this);
        this.toggle = this.toggle.bind(this);
        this.getSectionId = this.getSectionId.bind(this);
        // this.updateSection = this.updateSection.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        const trainingId = this.props.match.params.id;

        Axios.get(`/api/trainings/${trainingId}`)
            .then(response => {
                // console.log(response);
                if (this._isMounted) {
                    this.setState({
                        training: response.data,
                        sections: response.data.sections,
                        // chapters: response.data.sections.chapters,
                        section_title: '',
                        errors: []
                    });
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleFieldChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleAddNewSection = event => {
        event.preventDefault();
        const section = {
            section_title: this.state.section_title,
            training_id: this.state.training.id
        };
        // console.log(section);
        Axios.post('/api/sections', section)
            .then(response => {
                this.setState({
                    section_title: ''
                });
                this.setState(prevState => ({
                    sections: prevState.sections.concat(response.data)
                }));
                console.log(`Section store response in parent `, response.data);
                console.log(`State 'sections' in parent `, this.state.sections);
            })
            .catch(error => {
                this.setState({
                    errors: error.response.data.errors
                });
            });
    };

    hasErrorFor = field => {
        return !!this.state.errors[field];
    };

    renderErrorFor = field => {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback d-block'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    };

    /* TrainingSections Child Component */
    // Deletes a section
    handleSectionDelete = sectionId => {
        Axios.delete(`/api/sections/${sectionId}/destroy`)
            .then(response => {
                alert(response.data.message);
                this.setState({
                    sections: response.data.sections
                });
            })
            .catch(error => {
                console.log(error);
            });
    };

    toggle() {
        // console.log('toggle called');
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    getSectionId = sectionId => {
        this.toggle();
        this.setState({
            section_id: sectionId
        });
        // console.log(sectionId, this.state);
    };

    handleCreateChapter = title => {
        event.preventDefault();
        console.log(`Chapter title in T: `, this.state.chapter_title);
        const chapter = {
            title: title,
            section_id: this.state.section_id
        };

        this.setState({
            chapter_title: ''
        });
        console.log('chapter_title state refreshed');
        console.log('Chapter title in T after: ', this.state.chapter_title);

        this.toggle();
        // this.setState(prevState => ({
        //     chapter_title: prevState.chapter_title
        // }));

        Axios.post('api/chapters', chapter)
            .then(response => {
                // console.log(response.data);

                // Store response.data inside a variable
                var chapter = response.data;
                var sections = [...this.state.sections];

                // Find the index if the conditions match
                var index = sections.findIndex(
                    section => section.id === response.data.section_id
                );

                // Check if sections[index] has chapters? Create if id doesn't
                if (sections[index]['chapters']) {
                    sections[index]['chapters'].push(chapter);
                    // console.log('Found chapters in sections index');
                } else {
                    sections[index].chapters = [];
                    sections[index]['chapters'].push(chapter);
                }
                // console.log(`Section Chapter target`, sections);
                // console.log(`Stored in state:`, sections);
                this.setState({
                    sections: sections
                });
            })
            .catch(error => {
                if (error.response) {
                    // Populate the errors state
                    this.setState({
                        errors: error.response.data.errors
                    });
                }
            });
    };

    onDragEnd = result => {
        console.log(result);
        console.log(this.state.sections);
        const { destination, source, draggableId } = result;
        if (!destination) {
            return;
        }
        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ) {
            return;
        }
        var index = this.state.sections.findIndex(
            section => section.id === source.droppableId
        );
        console.log('index', index);
        const section = this.state.sections[index];
        console.log(`section`, section);

        const newChapterIds = Array.from(section.chapters);
        console.log(`newChapterIds before `, newChapterIds);

        newChapterIds.splice(source.index, 1);
        newChapterIds.splice(destination.index, 0, draggableId);
        console.log(`newChapterIds after`, newChapterIds);

        const newChapter = {
            ...section,
            chapters: newChapterIds
        };
        console.log(`newChapter`, newChapter);

        const newState = {
            ...this.state,
            chapters: {
                ...this.state.chapters,
                [newChapter.id]: newChapter
            }
        };
        console.log(`newState`, newState);

        // this.setState(newState)
    };

    render() {
        const { training, sections } = this.state;
        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <div className='container py-4'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className='card'>
                                <div className='card-header'>
                                    <h5 className='mb-0'>{training.title}</h5>
                                </div>
                                <div className='card-body'>
                                    <div className='row'>
                                        <div className='col'>
                                            {training.description ? (
                                                <TrainingDescContainer className='border bg-light p-3'>
                                                    {training.description}
                                                </TrainingDescContainer>
                                            ) : (
                                                <TrainingDescContainer>
                                                    <small className='text-muted font-italic'>
                                                        No description
                                                    </small>
                                                </TrainingDescContainer>
                                            )}
                                        </div>
                                    </div>
                                    <hr />
                                    <form onSubmit={this.handleAddNewSection}>
                                        <div className='d-flex flex-row align-items-start'>
                                            <div className='flex-grow-1'>
                                                <input
                                                    type='text'
                                                    name='section_title'
                                                    className={`form-control ${
                                                        this.hasErrorFor(
                                                            'section_title'
                                                        )
                                                            ? 'is-invalid'
                                                            : ''
                                                    }`}
                                                    id='section_title'
                                                    value={
                                                        this.state.section_title
                                                    }
                                                    onChange={
                                                        this.handleFieldChange
                                                    }
                                                    placeholder='Section Title'
                                                />
                                                {this.renderErrorFor(
                                                    'section_title'
                                                )}
                                            </div>
                                            <div>
                                                <button className='btn btn-primary'>
                                                    Add Section
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <hr />
                                    <TrainingSections
                                        sections={sections}
                                        deleteSection={this.handleSectionDelete}
                                        addChapter={this.handleCreateChapter}
                                        isModalOpen={this.state.isModalOpen}
                                        getSectionId={this.getSectionId}
                                        toggle={this.toggle}
                                    />
                                    <AddChapter
                                        buttonLabel='Add Chapter'
                                        modal={this.state.isModalOpen}
                                        toggle={this.toggle}
                                        addChapter={this.handleCreateChapter}
                                        chapterTitle={this.state.chapter_title}
                                        autoFocus={true}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </DragDropContext>
        );
    }
}

class AddChapter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.chapterTitle,
            errors: []
        };
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     // do things with nextProps.someProp and prevState.cachedSomeProp
    //     if (nextProps.chapterTitle !== prevState.title) {
    //         return {
    //             title: nextProps.chapterTitle
    //         };
    //     }
    //     return null;
    // }
    toggle() {
        // console.log('toggle called');
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleFieldChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    hasErrorFor = field => {
        return !!this.state.errors[field];
    };

    renderErrorFor = field => {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            );
        }
    };
    render() {
        // console.log(`Title in AddChapter: `, this.state.title);
        return (
            <div>
                {/* <Button
                    onClick={this.props.toggle}
                    color={this.props.color ? this.props.color : 'primary'}
                    className={this.props.btnClassname}
                    size={this.props.btnSize}
                    outline={this.props.btnOutline}
                    block={this.props.btnBlock}
                >
                    {this.props.buttonLabel}
                </Button> */}
                <Modal
                    isOpen={this.props.modal}
                    toggle={this.props.toggle}
                    className={this.props.className}
                    autoFocus={!this.props.autoFocus}
                >
                    <ModalHeader toggle={this.props.toggle}>
                        Add new chapter
                    </ModalHeader>
                    <form
                        onSubmit={this.props.addChapter.bind(
                            this,
                            this.state.title
                        )}
                    >
                        <ModalBody>
                            <div className='form-group'>
                                <label htmlFor='title'>Chapter Title</label>{' '}
                                <input
                                    type='text'
                                    name='title'
                                    id='title'
                                    className={`form-control ${
                                        this.hasErrorFor('title')
                                            ? 'is-invalid'
                                            : ''
                                    }`}
                                    value={this.state.title}
                                    onChange={this.handleFieldChange}
                                    autoFocus={this.props.autoFocus}
                                />
                                {this.renderErrorFor('title')}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color='primary' type='submit'>
                                Add
                            </Button>{' '}
                            {/* <Button color='secondary' onClick={this.toggle}>
                            Cancel
                        </Button> */}
                        </ModalFooter>
                    </form>
                </Modal>
            </div>
        );
    }
}
