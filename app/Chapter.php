<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = ['title', 'section_id', 'index'];

    public function items() {
        return $this->hasMany('App\Chapter_Items');
    }
}
