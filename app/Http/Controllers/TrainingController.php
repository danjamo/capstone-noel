<?php

namespace App\Http\Controllers;

use App\Training;
use Illuminate\Http\Request;

class TrainingController extends Controller
{
    public function index() {
        $trainings = Training::orderBy('created_at', 'desc')
        ->withCount(['sections'])
        ->get();
        return $trainings->toJson();
    }
    
    public function store(Request $request) {
        $validateData = $request->validate([
            'title' => 'required'
        ]);

        $training = Training::create([
            'title' => $validateData['title'],
        ]);

        return response($training->toJson(), 200);
    }

    public function show($id) {
        $training = Training::with(['sections.chapters'])->find($id);

        // $training = Training::find($id);

        // $training->sections->where('training_id', $id)->get();

        return $training->toJson();
    }
}
