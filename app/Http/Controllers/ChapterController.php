<?php

namespace App\Http\Controllers;

use App\Chapter;
use DB;
use Illuminate\Http\Request;

class ChapterController extends Controller
{
    public function store(Request $request) {
        $validateData = $request->validate([
            'title' => 'required'
        ]);

        // Get the last index of the section
        $lastIndex = Chapter::where('section_id', '=', $request->section_id)->get()->pluck('index')->last();

        // Check if $lastIndex is null or empty
        if($lastIndex === "" || $lastIndex === null ) {
            $lastIndex = 0;
        } else {
            $lastIndex += 1;
        }

        $chapter = Chapter::create([
            'title' => $validateData['title'],
            'section_id' => $request->section_id,
            'index' => $lastIndex
        ]);
        

        return response($chapter->toJson());
        // $chapter = Chapter::create([
        //     'title' => $validateData['title'],
        //     'section_id' => $request->section_id
        // ]);

        // DB::table('chapters')->where('section_id', $request->section_id)->increment('index');

        // $chapter = Chapter::

        // $chapter = Chapter::create([
        //     'title' => $request->,
        // ])
    }
}
