<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SectionController extends Controller
{
    public function store(Request $request) {
        $validateData = $request->validate(['section_title' => 'required']);

        $section = Section::create([
            'title' => $validateData['section_title'],
            'training_id' => $request->training_id
        ]);

        // $section->with('chapters')->get();

        return $section->toJson();
    }

    public function destroy($id) {
        // Get the training ID
        $getTrainingId = DB::table('sections')->where('id', '=', $id)->pluck('training_id')->first();

        $section = Section::find($id);
        $section->delete();

        $sections = DB::table('sections')->where('training_id', '=', $getTrainingId)->get();

        return response()->json([
            "message" => "Section successfully deleted", 
            "sections" => $sections
            ]);
    }
}
