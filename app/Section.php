<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Section extends Model
{
    protected $fillable = ['title', 'description', 'training_id'];

    public function chapters() {
        return $this->hasMany(Chapter::class)->orderBy('index', 'asc');
    }

    public function training() {
        return $this->belongsTo('App\Training');
    }
}
