<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('trainings', 'TrainingController@index');
Route::get('trainings/{id}', 'TrainingController@show');

Route::post('trainings', 'TrainingController@store');
Route::post('sections', 'SectionController@store');
Route::post('chapters', 'ChapterController@store');

Route::delete('sections/{id}/destroy', 'SectionController@destroy');
// Route::put('trainings/{training}', 'trainingController@markAsCompleted');
